module.exports = function(grunt) {
    // Configurable paths for the application
    var appConfig = {
      app: require('./bower.json').appPath || 'app',
      dist: 'dist'
    };
    var serveStatic = require('serve-static');


    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
            concat: {
                      options: {
                        // define a string to put between each file in the concatenated output
                        separator: ';'
                      },
                      dist: {
                        // the files to concatenate
                        src: ['app/**/*.js'],
                        // the location of the resulting JS file
                        dest: 'dist/<%= pkg.name %>.js'
                      }
                    },
                    uglify: {
                      options: {
                        // the banner is inserted at the top of the output
                        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
                      },
                      dist: {
                        files: {
                          'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                        }
                      }
                    },
                    qunit: {
                      files: ['app/**/*.html']
                    },
                    jshint: {
                      // define the files to lint
                      files: ['Gruntfile.js', 'app/**/*.js', 'test/**/*.js'],
                      // configure JSHint (documented at http://www.jshint.com/docs/)
                      options: {
                        // more options here if you want to override JSHint defaults
                        globals: {
                          jQuery: true,
                          console: true,
                          module: true
                        }
                      }
                    },
                    watch: {
                      bower: {
                        files: ['bower.json'],
                        tasks: ['wiredep']
                      },
                      js: {
                        files: ['app/scripts/{,*/}*.js'],
                        tasks: ['newer:jshint:all', 'newer:jscs:all'],
                        options: {
                          livereload: '<%= connect.options.livereload %>'
                        }
                      },
                      jsTest: {
                        files: ['test/spec/{,*/}*.js'],
                        tasks: ['newer:jshint:test', 'newer:jscs:test', 'karma']
                      },
                      gruntfile: {
                        files: ['Gruntfile.js']
                      },
                      sass: {
                            files: ['app/styles/{,*/}*.{scss,sass}'],
                            tasks: ['sass']
                      },
                      livereload: {
                        options: {
                          livereload: '<%= connect.options.livereload %>'
                        },
                        files: [
                          'app/{,*/}*.html',
                          'app/styles/{,*/}*.scss',
                          'app/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                        ]
                      }
                    },


                  sass: {                              // Task
                    dist: {                            // Target
                      options: {                       // Target options
                        style: 'expanded'
                      },
                      files: {                         // Dictionary of files
                        'app/styles/main.css': 'app/styles/main.scss'       // 'destination': 'source'
                      }
                    }
                },
                connect: {
                  options: {
                    port: 9000,
                    // Change this to '0.0.0.0' to access the server from outside.
                    hostname: '0.0.0.0',
                    livereload: 35729,
                    open: true
                  },

                  livereload: {
        options: {
          middleware: function(connect) {
            return [
              serveStatic('.tmp'),
              connect().use('/bower_components', serveStatic('./bower_components')),
              serveStatic(appConfig.app)
            ];
          }
        }
    },
                  test: {
                    options: {
                      port: 9001,
                      middleware: function (connect) {
                        return [
                          connect.static('.tmp'),
                          connect.static('test'),
                          connect().use(
                            '/bower_components',
                            connect.static('./bower_components')
                          ),
                          connect.static(appConfig.app)
                        ];
                      }
                    }
                  },
                  dist: {
                    options: {
                      open: true,
                      base: '/dist/'
                    }
                  }
              },
              wiredep: {
                app: {
                  src: ['app/index.html'],
                  ignorePath:  /\.\.\//
                },

                sass: {
                  src: ['app/styles/{,*/}*.{scss,sass}'],
                  ignorePath: /(\.\.\/){1,2}bower_components\//
                }
              }

    });

    // Loading all NPM tasks
    grunt.loadNpmTasks('grunt-wiredep');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-qunit');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');

    //
    // Creating Server Task
    grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
      if (target === 'dist') {
        return grunt.task.run(['build', 'connect:dist:keepalive']);
      }

      grunt.task.run([
        'wiredep',
        'sass',
        'connect:livereload',
        'watch'
      ]);
    });



    // Creating Grunt tasks
    grunt.registerTask('test', ['jshint','wiredep', 'qunit', 'sass','connect:livereload', 'watch']);

    grunt.registerTask('default', ['jshint', 'qunit', 'concat', 'uglify']);


};
